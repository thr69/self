<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<link href="{{ asset('css/list.css') }}" rel="stylesheet">
<!------ Include the above in your HEAD tag ---------->

<section class="head">
    <div class="container">
        <h2 class="text-center"><span style=""> لیست سفارشات ثبت شده </span></h2>
    </div>
</section>
<div class="clearfix"></div>
<section class="search-box">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 listing-block">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">reserve at</th>
                        <th scope="col">meal</th>
                        <th scope="col">resturant</th>
                        <th scope="col">food</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js"></script>
<script>
    $(function () {
        $('.listing-block').slimScroll({
            height: '600px'
        });
    });
    jQuery(document).ready(function () {
        var settings = {
            "url": "{{ route('reserve.list')}}",
            "method": "GET",
            "timeout": 0,
            "headers": {
                "Authorization": "Bearer " + localStorage.token
            },
        };

        $.ajax(settings).done(function (response) {
            console.log(response);
            $.each(response.list, function (i, r) {
                // $('body').prepend('<div class="alert alert-warning" role="alert" > ' + r + '</div>');
                $('tbody').append('<tr>' +
                    '<th scope="row">'+(i+1)+'</th>' +
                    '<td>'+r.reserve_at+'</td>' +
                    '<td>'+r.meal.name+'</td>' +
                    '<td>'+r.restaurant.name+'</td>' +
                    '<td>'+r.food.name+'</td>' +
                    '</tr>');
            });
        });

    });
</script>
