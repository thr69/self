<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">

</head>
<body>
<div class="container">

    <hr>

    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title mt-2">Reserve Form</h4>
                </header>
                <article class="card-body">
                    <form>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Restaurant</label>
                                <select id="Restaurant" name="restaurant_id" class="form-control">
                                    <option> Choose...</option>
                                </select>
                            </div> <!-- form-group end.// -->
                            <div class="form-group col-md-6">
                                <label>Meal</label>
                                <select id="Meal" name="meal_id" class="form-control">
                                    <option> Choose...</option>
                                </select>
                            </div> <!-- form-group end.// -->
                        </div> <!-- form-row.// -->
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Food</label>
                                <select id="Food" name="food_id" class="form-control">
                                    <option> Choose...</option>
                                </select>
                            </div> <!-- form-group end.// -->
                            <div class="form-group col-md-6">
                                <label>Reserve At</label>
                                <input type="datetime-local" name="reserve_at" class="form-control">
                            </div> <!-- form-group end.// -->
                        </div> <!-- form-row end.// -->
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block"> Reserve</button>
                        </div> <!-- form-group// -->
                    </form>
                </article> <!-- card-body end .// -->
            </div> <!-- card.// -->
        </div> <!-- col.//-->

    </div> <!-- row.//-->
</div>

<script src="{{ asset('js/jwt-decode.min.js') }}" defer></script>

<script>
    jQuery(document).ready(function () {
        var settings = {
            "url": "{{ route('reserve.menu') }}",
            "method": "GET",
            "timeout": 0,
            "headers": {
                "Authorization": "Bearer " + localStorage.token
            },
        };

        $.ajax(settings).done(function (response) {
            $.each(response.restaurant, function (i, r) {
                $('#Restaurant').append('<option value="' + r.id + '">' + r.name + '</option>');
            });

            $.each(response.meals, function (i, m) {
                $('#Meal').append('<option value="' + m.id + '">' + m.name + '</option>');
            });
        });

    });

    $(".btn-primary").click(function (event) {
        event.preventDefault();
        var settings = {
            "url": "{{ route('reserve.post') }}",
            "method": "POST",
            "timeout": 0,
            "enctype": 'multipart/form-data',
            "cache": false,
            "processData": false,
            "contentType": false,
            "headers": {
                "Authorization": "Bearer " + localStorage.token,
                "Content-Type": "application/json"
            },
            "data": JSON.stringify({
                "food_id": $("select[name=food_id]").val(),
                "meal_id": $("select[name=meal_id]").val(),
                "restaurant_id": $("select[name=restaurant_id]").val(),
                "reserve_at": $("input[name=reserve_at]").val()
            }),
            error: function (jqXHR, textStatus, errorThrown) {
                $.each(jqXHR.responseJSON.errors, function (i, e) {
                    $('body').prepend('<div class="alert alert-warning" role="alert" > ' + e + '</div>');
                });
                $('body').prepend('<div class="alert alert-danger" role="alert" > ' + jqXHR.responseJSON.message + '</div>');
            },
        };

        $.ajax(settings).done(function (response) {
            window.location.replace("{{ route('api.list')}}");
        });
    });

    $('#Meal').change(function () {

        var url = '{{ route("reserve.meal.food", ":id") }}';
        url = url.replace(':id', $("select[name=meal_id]").val());
        var settings = {
            "url": url,
            "method": "GET",
            "timeout": 0,
            "headers": {
                "Authorization": "Bearer " + localStorage.token
            },
        };

        $.ajax(settings).done(function (response) {
            $('#Food').empty();
            $.each(response.foods, function (i, f) {
                $('#Food').append('<option value="' + f.id + '">' + f.name + '</option>');
            });
        });
    });

</script>
</body>


