<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="{{ asset('css/login.css') }}" rel="stylesheet" >
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>

<!------ Include the above in your HEAD tag ---------->

<div class="container login-container">
    <div class="row justify-content-center">
        <div class="col-md-6 login-form-2">
            <h3>Login</h3>
            <form>
                <div class="form-group">
                    <input name="email" type="text" class="form-control" placeholder="Your Email *" value="" />
                </div>
                <div class="form-group">
                    <input name="password" type="password" class="form-control" placeholder="Your Password *" value="" />
                </div>
                <div class="form-group">
                    <input type="submit" class="btnSubmit" value="Login" />
                </div>
                <div class="form-group">

                    <a href="#" class="ForgetPwd" value="Login">Forget Password?</a>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(".btnSubmit").click(function(event){
        event.preventDefault();
        let password = $("input[name=password]").val();
        let email = $("input[name=email]").val();
        var settings = {
            "url": "{{ route('api.login')}}",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "accept": "application/json",
                "Content-Type": "application/json"
            },
            "data": JSON.stringify({"email":email,"password":password}),
        };

        $.ajax(settings).done(function (response) {
            localStorage.token=response.access_token
            window.location.replace("{{ route('api.home')}}");
        });
    });
</script>
