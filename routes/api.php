<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'ApiController@register');
Route::get('login', 'ApiController@viewlogin')->name('apilogin');
Route::post('login', 'ApiController@login')->name('api.login');
Route::get('logout', 'ApiController@logout');
Route::get('logout', 'ApiController@logout');

Route::middleware('auth:api')->name('reserve.')->group(function () {

    Route::get('user', 'ApiController@getAuthUser');

    Route::get('reserve', 'ReserveController@index')->name('menu');
    Route::post('reserve', 'ReserveController@Store')->name('post');
    Route::delete('reserve/{id}', 'ReserveController@delete');
    Route::get('reserve/{id}', 'ReserveController@Show');
    Route::put('reserve/{id}', 'ReserveController@update');
    Route::get('reserves/list', 'ReserveController@list')->name('list');
    Route::delete('delete', 'ReserveController@delete');

    Route::get('meal/food/{id}', 'ReserveController@mealsfood')->name('meal.food');

});

Route::name('api.')->group(function () {

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/list', 'HomeController@list')->name('list');

});









