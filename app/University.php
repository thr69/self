<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    protected $table = 'university';

    public function users()
    {
        return $this->belongsToMany('App\User', 'university_user',
            'university_id', 'user_id');
    }

    public function reserves()
    {
        return $this->hasMany('App\Restaurant');
    }
}
