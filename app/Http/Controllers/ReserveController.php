<?php

namespace App\Http\Controllers;

use App\Food;
use App\Foods;
use App\Meal;
use App\Reserve;
use App\Restaurant;
use App\User;
use Illuminate\Http\Request;
use Carbon;
use Illuminate\Validation\Rules\Exists;

class ReserveController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = User::find(auth()->id());

        $foods = Food::select('id', 'name')->get();
        $meals = Meal::select('id', 'name')->get();
        $Restaurant = Restaurant::where('university_id',$user->university->first()->id)->select('id', 'name')->get();

        return response()->json([
            'foods' => $foods,
            'meals' => $meals,
            'restaurant' => $Restaurant
        ], 201);

    }

    public function Store(Request $request)
    {
        try {

            $validator =  $request->validate([
                'food_id' => ['required','exists:App\Food,id', 'numeric'],
                'meal_id' => ['required','exists:App\Meal,id', 'numeric'],
                'restaurant_id' => ['required','exists:App\Restaurant,id', 'numeric'],
                'reserve_at' => ['required'],
            ]);

            if (Carbon\Carbon::now()->addDay('7')  > $request->reserve_at ){
                return response()->json([
                    'message' => 'Your Date Is Not Valid!',
                ], 422);
            }
            Reserve::create([
                    "user_id"=> auth()->id(),
                    "food_id"=>$request->food_id,
                    "meal_id"=>$request->meal_id,
                    "restaurant_id"=>$request->restaurant_id,
                    "reserve_at"=>$request->reserve_at,
                ]);

            return response()->json([
                'message' => 'Your food reservation was successful!',
            ], 201);


        } catch (\Exception $e) {
                return response()->json([
                    'message' => 'Your food reservation has encountered an error!',
                    'errorcode' => $e,
                    'errors' => $e->errors(),
                ], 422);

        }

    }

    public function update(Request $request,$id)
    {

        try {

            $validator =  $request->validate([
                'food_id' => ['required','exists:App\Food,id', 'numeric'],
                'meal_id' => ['required','exists:App\Meal,id', 'numeric'],
                'restaurant_id' => ['required','exists:App\Restaurant,id', 'numeric'],
                'reserve_at' => ['required'],
            ]);

            if (Carbon\Carbon::now()->addDay('7')  > $request->reserve_at ){
                return response()->json([
                    'message' => 'Your Date Is Not Valid!',
                ], 422);
            }
            Reserve::where('id', $id)
                ->update([
                "user_id"=> auth()->id(),
                "food_id"=>$request->food_id,
                "meal_id"=>$request->meal_id,
                "restaurant_id"=>$request->restaurant_id,
                "reserve_at"=>$request->reserve_at,
            ]);

            return response()->json([
                'message' => 'Your food reservation update successful!',
            ], 201);


        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Your food reservation has encountered an error!',
                'errorcode' => $e->getCode(),
                'errors' => $e->errors(),
            ], 422);

        }

    }

    public function Show($id)
    {
        try {
            return response()->json([
                'reserve' => User::find(auth()->id())->reserves()->list()->find($id)
            ], 201);

        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Your food reservation has encountered an error!',
                'errorcode' => $e->getCode(),
                'errors' => $e->errors(),
            ], 422);

        }

    }

    public function list()
    {

        $user = User::find(auth()->id());
        $reserves = $user
            ->reserves()
            ->list()
            ->with(['food','meal','restaurant'])
            ->get();
        return response()->json([
            'list' => $reserves,
        ], 201);
    }

    public function delete(Reserve $reserve){

        try {

        $reserve->delete();

        return response()->json([
            'message' => 'Deleted Successfully !',
        ], 201);

        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Your food reservation has encountered an error!',
                'errorcode' => $e->getCode(),
                'errors' => $e->errors(),
            ], 422);
        }
    }

    public function mealsfood($meal)
    {
        return response()->json([
            'foods' => Meal::whereId($meal)->first()->food,
        ], 201);

    }

}
