<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    public function food()
    {
        return $this->belongsTo('App\University')->select('id','name');
    }

}
