<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meal extends Model
{
    public function food()
    {
        return $this->belongsToMany('App\Food', 'food_meal',
            'meal_id', 'Food_id');
    }
}
