<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reserve extends Model
{
    use SoftDeletes;
    protected $table = 'reserve';

    protected $fillable = [
        'user_id', 'food_id', 'meal_id','restaurant_id','reserve_at'
    ];

    public function users()
    {
        return $this->belongsTo('App\User');
    }

    public function food()
    {
        return $this->belongsTo('App\Food','food_id')->select('id','name');
    }

    public function meal()
    {
        return $this->belongsTo('App\Meal','meal_id')->select('id','name');
    }

    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant','restaurant_id')->select('id','name');
    }

    public function scopeList($query)
    {
        return $query->select('id','reserve_at','food_id','restaurant_id','meal_id');
    }

}
