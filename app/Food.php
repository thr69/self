<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    protected $table = 'foods';

    public function meal()
    {
        return $this->belongsToMany('App\Meal', 'food_meal',
            'food_id', 'meal_id');
    }

}
